package com.example.PoonamWork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoonamWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoonamWorkApplication.class, args);
	}
}
