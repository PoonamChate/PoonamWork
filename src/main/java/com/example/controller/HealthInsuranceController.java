package com.example.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Person;

@RestController
public class HealthInsuranceController {
	
	@RequestMapping(value = "/information/", method = RequestMethod.POST)
    public void calculateInsurance(@RequestBody Person person) {
		
		double premium= 5000;
   
		if((person.getAge()>= 18 && person.getAge()< 25) ||
		   (person.getAge()>= 25 && person.getAge()< 30) ||
		   (person.getAge()>= 30 && person.getAge()< 35) ||
		   (person.getAge()>= 35 && person.getAge()< 40)  ) {
			
			premium = premium+(premium*10/100);
		}
		if(person.getAge()>= 40) {
			premium = premium+(premium*20/100);
		}
		if(person.getGendar().equals("male")) {
			
			premium = premium+(premium*2/100);
		}
		if(person.getCurrentHealth().getBloodPressure().equals("Yes")) {
			premium = premium+(premium*1/100);
		}
		if(person.getCurrentHealth().getBloodSugur().equals("Yes")) {
			premium = premium+(premium*1/100);
		}
		if(person.getCurrentHealth().getHypetension().equals("Yes")) {
			premium = premium+(premium*1/100);
		}
		if(person.getCurrentHealth().getOverweight().equals("Yes")) {
			premium = premium+(premium*1/100);
		}
		if(person.getHabits().getAlcohol().equals("Yes")) {
			premium = premium+(premium*3/100);
		}
		if(person.getHabits().getDailyExercise().equals("Yes")) {
			premium = premium-(premium*3/100);
		}
		if(person.getHabits().getDrugs().equals("Yes")) {
			premium = premium+(premium*3/100);
		}
		if(person.getHabits().getSmoking().equals("Yes")) {
			premium = premium+(premium*3/100);
		}
		System.out.println("Health Insurance Premium for Mr. Gomes: "+premium);
    }
}
