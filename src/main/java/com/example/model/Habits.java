package com.example.model;

public class Habits {

	String smoking;
	String alcohol;
	String dailyExercise;
	String Drugs;
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return Drugs;
	}
	public void setDrugs(String drugs) {
		Drugs = drugs;
	}
	
}
