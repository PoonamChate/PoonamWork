package com.example.model;

public class CurrentHealth {
	
	String hypetension;
	String bloodPressure;
	String bloodSugur;
	String overweight;
	public String getHypetension() {
		return hypetension;
	}
	public void setHypetension(String hypetension) {
		this.hypetension = hypetension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugur() {
		return bloodSugur;
	}
	public void setBloodSugur(String bloodSugur) {
		this.bloodSugur = bloodSugur;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	

}
