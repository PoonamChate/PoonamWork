package com.example.PoonamWork;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.controller.HealthInsuranceController;
import com.example.model.Person;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HealthInsuranceControllerTest {
	
	HealthInsuranceController healthInsuranceController  = new HealthInsuranceController();
	Person person = new Person();
	
	@Test
	public void calculateInsuranceTest() {
		
		healthInsuranceController.calculateInsurance(person);
		Assert.assertNotNull(person);
	}

}
